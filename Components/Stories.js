import React, {Component} from 'react';
import { StyleSheet, Text, View,ViewTouchableHighlight,Alert,TextInput,Button,Switch,FlatList,Image,TouchableOpacity,Dimensions} from 'react-native';
import {AppRegistry} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createDrawerNavigator,DrawerActions} from 'react-navigation'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const stories = [
  { key: 'Name Surname',
    time:'1h ago',
    description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor #incididunt ero labore et dolore magna aliqua. '
  }, 
  { key: 'Name Surname',
    time:'1h ago',
    description:'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor #incididunt ero labore et dolore magna aliqua. '
  }, 
  
];
export default class StoriesCell extends Component{
  
    
  
  
  _renderItem = ({ item }) => {
    return (
      <View style={{marginLeft:25,marginRight:25}}>
        <View style={{flexDirection:'row',flex:1,alignItems:'center'}}>
          <View style={{flexDirection:'row',flex:1,alignItems:'center'}}>
            <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}
                style={{width: 40, height: 40,borderRadius:40/2,alignSelf:'center'}} /> 
            <Text style={[styles.buttonText,{marginLeft:10,fontWeight:'bold'}]}>{item.key}</Text>
          </View>  
          <View style={{flexDirection:'row',flex:1,justifyContent:'flex-end',alignItems:'center'}}>
            <MaterialIcon name='query-builder' size={18} style={styles.buttonIcon}/>
            <Text style={[styles.buttonText]}>{item.time}</Text>
          </View>    
        </View>     
        <Text style={styles.description} >{item.description}</Text>
        <Image  style={styles.image} resizeMode='stretch'/>
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => Alert.alert('Alert', "908 people liked it!!")}>
            <Ionicons name='ios-heart' size={18} style={styles.buttonIcon} />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>908</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonContainer} >
            <MaterialIcon name='comment' size={18} style={styles.buttonIcon} onPress={() => Alert.alert('Alert', "456 comments!!")} />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>456</Text>
          </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText]}> SHARE</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  static navigationOptions = ({ navigation }) => {
    return {
        headerTitle: 'STORIES',
        headerTitleStyle:{ color:'#0097e6'},
        headerLeft:(
          <View style={{margin:10}}>
            <MaterialIcon
                onPress={navigation.toggleDrawer}
                name="menu"
                size={25}
                color='#0097e6'
            />
            </View>
        ),
        headerRight:
       ( <View style={{margin:20}}>
        <MaterialIcon
            onPress={navigation.toggleDrawer}
            name="search"
            size={25}
            color='#0097e6'
        />
        </View>)
    }
  }
  render() {
    return(
      <View style={styles.container}>
      <View style={styles.postContainer}>
      <MaterialIcon name='edit' size={18} style={alignSelf='flex-start'} color='#466FF7'/>
       <Text style={{color:'#8E8E8E',margin:5,fontSize:18,fontWeight:"100"}}>Write a post...</Text>
       </View>
            <FlatList
              data={stories}
              renderItem={this._renderItem}
            />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  postContainer:
  {
      borderWidth:1,
      backgroundColor:'#EFEFEF',
      margin:15,
      flexDirection:'row',
      alignItems:'center'
  },
    image: {
      width:null,
      height: Dimensions.get('window').height/3,
      alignContent: 'stretch',
      backgroundColor:'grey',
      marginTop:20,
      marginBottom:20
    },
    description: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
      fontSize: 18,
      color: '#3D86C6',
      marginTop:10
    },
    buttonsRow:
    {
      flexDirection: 'row',
      marginBottom:30,
    },
    likeAndComment:
    {
      flex: 1,
      flexDirection: 'row',
    },
    share:
    {
      alignSelf:'center',
    },
    buttonContainer:
    {
      flexDirection: 'row',
      alignItems: 'center',
      alignContent: 'center',
    },
    buttonIcon:
    {
      margin: 5,
      color: '#3D86C6',
    },
    buttonText:
    {
      fontSize: 18,
      color: '#3D86C6',
      marginRight:10
    },
  } 
);
