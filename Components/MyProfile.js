import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Image, ScrollView } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
export default class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        { key: 'PERSONAL INFORMATION' },
        { key: 'NOTIFICATIONS' },
        { key: 'PREFERENCES' },
        { key: 'TERMS & CONDITION' }
      ],
    }
  }

  static navigationOptions = ({ navigation }) => {

    return {
      headerTitle: 'MY PROFILE',
      headerTitleStyle: { color: '#3D86C6',flex:1,textAlign:'center' },
      headerStyle: {
        shadowOpacity: 10,
        backgroundColor: '#ebf7ff'
      },
      headerLeft: (
        <TouchableOpacity style={{ padding: 10 }} onPress={navigation.toggleDrawer}>
          <MaterialIcon
            name="menu"
            size={25}
            color='#3D86C6'
          />
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity style={{ padding: 20 }} onPress={()=>navigation.navigate('Search')}>
          <MaterialIcon
            name="search"
            size={25}
            color='#3D86C6'
          />
        </TouchableOpacity>)
    }
  }
  _renderItem = ({ item }) => {
    return (
      <TouchableOpacity style={{ flex: 1, flexDirection: 'row', paddingTop: 20, paddingBottom: 20 }}>

        <Text style={{ flex: 1, fontSize: 18, color: 'black', marginLeft: 20, alignSelf: 'center' }}>
          {item.key}
        </Text>
        <MaterialIcon name='chevron-right' size={30} style={{ marginRight: 20, alignSelf: 'center', color: '#596275' }} />
      </TouchableOpacity>
    )
  }

  renderSeperator = () => {
    return (
      <View style={{ width: '100%', height: 5, backgroundColor: '#E3E1E1', alignSelf: 'center' }}>
      </View>
    )
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', margin: 20 }}>
            <Image style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: 'grey' }} />
            <View style={{ flex: 1, flexDirection: 'column', alignSelf: 'center', marginLeft: 10, }}>
              <Text style={{ fontSize: 20 }}>Welcome Jane</Text>
              <Text style={{ fontSize: 16, color: '#8E8E8E' }}>Manager - DIAS, Mumbai. </Text>
            </View>
            <MaterialIcon name='edit' size={30} color='#3D86C6' style={{ alignSelf: 'center' }} />
          </View>
          <View style={{ flex: 1, flexDirection: 'row', margin: 10, alignSelf: 'center' }}>
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
              <MaterialIcon name='people' size={30} color='#3D86C6' style={{ alignSelf: 'center' }} />
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>20 </Text>
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>Followers</Text>
            </View>
            <View style={{ height: '80%', width: 1, backgroundColor: 'grey', alignSelf: 'center' }} />

            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
              <MaterialIcon name='comment' size={30} color='#3D86C6' style={{ alignSelf: 'center' }} />
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>173 </Text>
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>Comments</Text>
            </View>
            <View style={{ height: '80%', width: 1, backgroundColor: 'grey', alignSelf: 'center' }} />

            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
              <MaterialIcon name='edit' size={30} color='#3D86C6' style={{ alignSelf: 'center' }} />
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>4 </Text>
              <Text style={{ fontSize: 18, color: '#8E8E8E' }}>Posts</Text>
            </View>
          </View>
          <this.renderSeperator />
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            ItemSeparatorComponent={this.renderSeperator}
            scrollEnabled={false}
          />
          <this.renderSeperator />
        </ScrollView>

        <Text style={{ color: '#3D86C6', fontSize: 18, textAlign: 'center', margin: 10, fontWeight: 'bold' }}>SIGN OUT</Text>

      </View>

    )
  }
}