import React, {Component} from "react";
import {createAppContainer,createStackNavigator,DrawerNavigator,createDrawerNavigator,DrawerItems,NavigationActions,DrawerActions} from 'react-navigation'
import{
    View,
    Text,
    StyleSheet,
    SafeAreaView,Dimensions,ScrollView,TouchableOpacity,Alert
}from "react-native";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FeaturesComponent from "./FeaturesComponent";
import NotificationCell from "./NotificationCell";
import Events from "./Events";
import MyProfile from "./MyProfile";
import CustomerSuccess from "./CustomerSuccess";
import Media from "./Media";
import StoriesCell from "./Stories";
import Group from "./D21Group";

class customComponent extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            menu:'Summary'
        }}

    navigateToScreen = (route) =>  {
       const navigateAction = NavigationActions.navigate({
         routeName: route
       });
        this.state.menu=route
        console.log(this.state.menu);
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      }
  render() {
    return (
    <SafeAreaView style={{flex:1}}>
    <View style = {styles.container1}>
    </View> 
    <ScrollView style = {styles.container2}>
        <TouchableOpacity  style={[styles.drawerItemContainer,{marginBottom:-5},(this.state.menu =='Summary' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Summary')
        }}>
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}  >DESTINATION 21 SUMMARY</Text>
           </TouchableOpacity>
           <TouchableOpacity  style={[styles.subItemContainer,(this.state.menu =='CustomerSuccess' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('CustomerSuccess')
        }}>
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>CUSTOMER SUCCESS</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EMPLOYEE SUCCESS</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text  style={styles.drawerItemText}>REVENUE & PRODUCTIVITY IN THE YEAR 2019</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EBIDTA & ROCE</Text>
           </TouchableOpacity>
           <TouchableOpacity style={[styles.subItemContainer,{marginBottom:10}]} >
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>FINAL DESTINATION</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='Stories' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Stories')
        }}>
           <MaterialIcon name='dashboard' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>STORIES</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='D21Group' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('D21Group')
        }}>
           <MaterialIcon name='forum' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>D21 GROUP</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='Event' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Event')
        }}>
           <MaterialIcon name='explore' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EVENTS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='Media' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Media')
        }}>
           <MaterialIcon name='photo' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>MEDIA</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='Notification' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Notification')
        }}>
           <MaterialIcon name='mail' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>NOTIFICATIONS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity style={styles.drawerItemContainer} >
           <MaterialIcon name='videocam' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>DOWNLOADS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(this.state.menu =='MyProfile' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('MyProfile')
        }}>
           <MaterialIcon name='settings' size={25} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>MY PROFILE</Text>
           </TouchableOpacity>
    </ScrollView>
    </SafeAreaView>
    )}
}

const SummaryStack = createAppContainer(createStackNavigator(
    {
    first:FeaturesComponent,
    },
    {
      initialRouteName : 'first',
      //headerMode:'None'
    }
  ) )
  const NotificationStack = createAppContainer(createStackNavigator(
    {
    Notification:NotificationCell
    },
    {
      initialRouteName : 'Notification',
    }
  ) )
  const EventsStack = createAppContainer(createStackNavigator(
    {
    EventsList:Events
    },
    {
      initialRouteName : 'EventsList',
    }
  ) )
  const CustomerSuccessStack = createAppContainer(createStackNavigator(
    {
    CustomerSuccess:CustomerSuccess
    },
    {
      initialRouteName : 'CustomerSuccess',
    }
  ) )
  const MyProfileStack = createAppContainer(createStackNavigator(
    {
     MyProfile:MyProfile
    },
    {
      initialRouteName : 'MyProfile',
    }
  ) )
  const MediaStack = createAppContainer(createStackNavigator(
    {
     Media:Media
    },
    {
      initialRouteName : 'Media',
    }
  ) )
  const StoriesStack = createAppContainer(createStackNavigator(
    {
     Stories:StoriesCell
    },
    {
      initialRouteName : 'Stories',
    }
  ) )
  const D21GroupStack = createAppContainer(createStackNavigator(
    {
     D21Group:Group
    },
    {
      initialRouteName : 'D21Group',
    }
  ) )

const drawerNavigator = createDrawerNavigator(
    {
   Summary:SummaryStack,
   Notification:NotificationStack,
   Event:EventsStack,
   CustomerSuccess:CustomerSuccessStack,
   MyProfile:MyProfileStack,
   Media:MediaStack,
   Stories:StoriesStack,
   D21Group:D21GroupStack

    },
{     
        initialRouteName:'Summary',
        contentComponent: customComponent,
        contentOptions:{
        activeTintColor:'#3BB9FF',
        drawerWidth: Dimensions.get('window').width / (4/3),
      },
    },
)

const  AppContainer = createAppContainer(drawerNavigator)
export default AppContainer;

 const styles = StyleSheet.create({
     container:{
         backgroundColor: '#FFFFFF',
         color:'#FFFFFF',
         flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container1:{
        backgroundColor: '#3D86C6',
        height: 150,
    },
    title:{
        color: '#F5FCFF',
    },
    container2:{
        backgroundColor: '#202020'
    },
    drawerItemContainer:
    {
      flex:1,
      flexDirection:'row',
      padding:7,
      alignItems:'center',
     
    },
    drawerItemText:
    {   flex:1,
        flexWrap: 'wrap',
        color:'#ffffff',
        fontSize:15,
    },
    drawerItemIcon:
    {
        color:'#ffffff',
        margin:10
    },
    subItemContainer:
    { 
        flex:1,
        flexDirection:'row',
        paddingLeft:35,
        alignItems:'center',
        paddingRight:5,
    },
    ItemSeperator:
    {
        backgroundColor:'#3D86C6',//primary color
        height:0.5
    },
    active:
    {
        backgroundColor:'#34495e'
    }
});
