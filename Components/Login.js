import React, {Component} from 'react';
import {View,TextInput,Text,Button,StyleSheet,KeyboardAvoidingView} from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import { ScrollView } from 'react-native-gesture-handler';

export default class Login extends Component{
  render()
  {
    return(
     
      <View style={styles.container}>
      <KeyboardAvoidingView style={styles.container}  behaviour='position'>
      
         <View style={styles.header}>
         <Text style={styles.text}>APP TITLE HERE</Text>
        </View>
        </KeyboardAvoidingView>
         <View style={styles.container2}>
         <ScrollView>
          <View style={styles.content}> 
      <Text style={{fontSize:17,color:'#2183FA'}}>Username</Text>
      <TextInput style = {styles.input}></TextInput>
      <Text style={{fontSize:17,color:'#2183FA'}}>Password</Text>
      <TextInput style = {styles.input} secureTextEntry={true}></TextInput>
      <Text style={{fontSize:20,color:'#2183FA'}}>Forgot password?</Text>
      </View>
      </ScrollView>
      </View >
      <View style={{flex:1,margin:15,fontSize:20,justifyContent:'center'}}><Button title='LOGIN' style={{backgroundColor:'#4C97FA',fontSize:25}}
      onPress={() => { this.props.navigation.navigate('MyDrawerNavigation')
          }}/>
          
      </View>
      <View style={{flex:1,justifyContent:'center',backgroundColor:'#C3DFFA'}}>
      <Text style={{textAlign:'center',fontSize:20,color:'#2183FA'}}>Create an Account</Text>
      </View>
      </View>
    
   )

    
  }
}
const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'#F2F9FE',
      
    },
    header:
    {   alignItems:'center',
        justifyContent:'center',
        flex:1,
        height:40,
        backgroundColor:'#4C97FA',
        
    },
    text:
    {
      fontSize:20,
      color:'white',
      
    },
    input: {
        fontSize:20,
        marginBottom:25,
        borderColor: '#2183FA',
        borderWidth: 1,
        color:'#2183FA'
     },
     content:
     {
        margin: 15,
        
        
     },
     container2:
     {   flex:5,
         height:40,
         justifyContent:'center',
         
     }
    
  });