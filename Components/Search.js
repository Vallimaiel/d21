import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,TextInput} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';


export default class Search extends Component
{
    render(){
        return(
<View style={{flex:1,padding:20}}>
<View style={{flexDirection:'row',borderWidth:1,borderColor:'grey'}}>
<MaterialIcon name='search' size={25} color='grey' style={{alignSelf:'center',margin:5}}/>
    <TextInput placeholder='Type Here' style={{fontSize:18}}/>
    </View>
    <TouchableOpacity style={{backgroundColor:'#3D86C6',borderRadius:5,marginTop:30,padding:10}}>
    <Text style={{alignSelf:'center',fontSize:20,color:'white'}}>Search</Text>
    </TouchableOpacity>
</View>
        )
    }
}