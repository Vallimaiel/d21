import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,FlatList,Image,ScrollView} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';



export default class NotificationCell extends Component
{
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'NOTIFICATIONS',
            headerTitleStyle:{ color:'#3D86C6',flex:1,textAlign:'center'},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:'#ebf7ff'
            },
            headerLeft:(
              <TouchableOpacity style={{padding:10}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={25}
                    color='#3D86C6'
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{padding:20}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={25}
                color='#3D86C6'
            />
            </TouchableOpacity>)
        }
      }
    
    
render()
{
   
    return(
        <ScrollView style={{flex:1}}>
        <View style={{flex:1,flexDirection:'row',margin:20}}>
            <Image style={{backgroundColor:'grey',width:80,height:80,marginTop:5}}/>
            <View style={{flex:1,marginLeft:10}}>
            <Text style={{textAlign:'left',color:'#8E8E8E',fontSize:14,fontStyle:'italic'}}>1h ago</Text>
            <Text  numberOfLines={2} style={{alignSelf:'center',fontSize:16,color:'#3D86C6',lineHeight:30}}  >Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim </Text>
            </View> 
        </View>
        <View style={{ width: '100%', height: 1, backgroundColor: '#E3E1E1', alignSelf: 'center' }}/>
        <View style={{flex:1,flexDirection:'row',margin:20}}>
            <Image style={{backgroundColor:'grey',width:80,height:80,marginTop:5}}/>
            <View style={{flex:1,marginLeft:10}}>
            <Text style={{textAlign:'left',color:'#8E8E8E',fontSize:14,fontStyle: 'italic'}}>1h ago</Text>
            <Text  numberOfLines={2} style={{alignSelf:'center',fontSize:16,color:'#3D86C6',lineHeight:30}}  >Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim </Text>
            </View> 
        </View>
        <View style={{ width: '100%', height: 1, backgroundColor: '#E3E1E1', alignSelf: 'center' }}/>
        </ScrollView>
    );
}
}