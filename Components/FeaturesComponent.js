import React, { Component } from 'react';
import {
  StyleSheet, Text, View, ViewTouchableHighlight, Alert, FlatList, Image
  , ActivityIndicator, TouchableOpacity, ToastAndroid, ScrollView
} from 'react-native';
import { createStackNavigator,createAppContainer } from 'react-navigation'
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';


const backgroundColor='#ffffff'//#e6f7ff,#ebf7ff

const data = [
  { key: 'CUSTOMER SUCESS',key2: 'Target 70' }, 
  { key: 'EMPLOYEE SUCCESS',key2: 'Target 100' },
  { key: 'REVENUE & PRODUCTIVITY',key2: 'Target 15% + 15%'},
  { key: 'EBIDTA & ROCE',key2: 'Target 25% + 25%' },
  { key: 'FINAL DESTINATION',key2: 'Target Top 10th %' }
];
const stories = [
  { key: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
    time:'1h ago'
  }, 
  { key: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
  time:'1h ago' }, 
  { key: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
  time:'1h ago' } 
  
];
const numColumns = 2;

export default class FeaturesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      isLoading: true,
      itemCount: 2,
    }
  }

  static navigationOptions = ({ navigation }) => {

    return {
        headerTitle: 'D21 SUMMARY',
        headerTitleStyle:{ color:'#3D86C6',flex:1,textAlign:'center'},
        headerStyle: {
             shadowOpacity: 10,
            // elevation:10,
             backgroundColor:'#ebf7ff'
        },
        headerLeft:(
          <TouchableOpacity style={{padding:10}} onPress={navigation.toggleDrawer}>
            <MaterialIcon
                name="menu"
                size={25}
                color='#3D86C6'
            />
            </TouchableOpacity>
        ),
        headerRight: (
           <TouchableOpacity style={{padding:20}} onPress={()=>navigation.navigate('Search')}>
        <MaterialIcon
            name="search"
            size={25}
            color='#3D86C6'
        />
        </TouchableOpacity>)
    }
  }

  renderItem = ({ item, index }) => {
    return (
//       <View style ={{backgroundColor: index %2 == 0 ? '#E3E1E1' : 'white',
//       flex: 1,
//       alignItems:'center',
//       justifyContent:'center',
//       flexDirection:'row',
//       height: 160 / numColumns,
//       marginBottom: 15,
//       elevation:2,
//       shadowOpacity:1,
//       borderRadius:5
      
//       }}>
//       {index%2==0?
//       <MaterialIcon name='face' size={30} style={{alignSelf:'center'}}/>
//       :null}
//          <View style={{flex:1}}>
//             <Text style={{color:'black',textAlign:'center'}}>{item.key}</Text>
//          </View> 
//      {index%2==1?
//      <MaterialIcon name='chevron-right' size={30} style={{alignSelf:'center'}}/>
//      :null}
// </View>
<View style={{flex:1 ,alignItems:'center',
      justifyContent:'center',
      flexDirection:'row',
      marginBottom: 15,
      elevation:2,
      shadowOpacity:2,
      borderRadius:5,
      height:80}}> 
      <View style={{flex:1,backgroundColor:'#E3E1E1',borderBottomLeftRadius:5,borderTopLeftRadius:5}}>
      <View style={{flex:1,flexDirection:'row',marginLeft:15,marginRight:15,flexWrap:'wrap'}}> 
        <MaterialIcon name='face' size={30} style={{alignSelf:'center'}}/>
      <Text style={{flex:1,color:'black',alignSelf:'center',flexWrap:'wrap',textAlign:'left',marginLeft:10}}>{item.key}</Text>
         </View>
         </View>
          <View style={{flex:1,backgroundColor:'white',flexDirection:'row',flexWrap:'wrap',padding:15}}>
          <Text style={{textAlign:'center',alignSelf:'flex-start',color:'#8E8E8E'}}>{item.key2}</Text>
       </View>
       <MaterialIcon name='chevron-right' size={30} style={{alignSelf:'center',color:'black'}}/>
</View>
    );
  };

  renderItemStories = ({ item, index }) => {
    return (
      <View style ={{
      flex: 1,
      marginLeft: 20,
      marginRight:20,
      marginBottom:10,
      marginTop:2}}>
        <Text  numberOfLines={2} style={{fontSize:16,color:'#3D86C6',lineHeight:30}}  >{item.key} </Text>
        <Text style={{textAlign:"right",color:'#3D86C6',marginTop:15,marginBottom:10,fontSize:14}}>{item.time}</Text>
        <View style={{ width: '100%', height: 1, backgroundColor: '#E3E1E1', alignSelf: 'center' }}>
         </View>
      </View>
    );
  };

  _renderItem = ({ item }) => {
    return (
      <View style={{}}>
        <TouchableOpacity style={styles.imageContainer}
          onPress={
            () => {
              ToastAndroid.show(item.book_title, ToastAndroid.SHORT)
              // this.props.navigation.navigate('Detail', { itemId: item.book_title, value: item.author })
            }
          }>
          <Image source={{ uri: item.image }} resizeMode="stretch" style={styles.image} />
          <Text numberOfLines={2} style={styles.description} >{item.book_title}</Text>

        </TouchableOpacity>
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => Alert.alert('Alert', "908 people liked it!!")}>
            <Ionicons name='ios-heart' size={18} style={styles.buttonIcon} />
            <Text style={styles.buttonText}>908</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonContainer} >
            <MaterialIcon name='comment' size={18} style={styles.buttonIcon} onPress={() => Alert.alert('Alert', "456 comments!!")} />
            <Text style={styles.buttonText}>456</Text>
          </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText]}> SHARE</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderSeperator = () => {
    return (
      <View style={{ width: '80%', height: 1, backgroundColor: '#E3E1E1', alignSelf: 'center' }}>
      </View>
    )
  }
  myKeyExtractor = (item, index) => item.book_title;

  componentDidMount() {

    const url = 'http://www.json-generator.com/api/json/get/ccLAsEcOSq?indent=1'
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson.book_array,
          isLoading: false
        })
      })
      .catch((error) => {
        console.log(error)
      })
     
  }
  render() {
    const {
      renderHeader,
    } = this.state;
      return (
        this.state.isLoading ?
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size='large' color='#330066' ></ActivityIndicator>
          </View> :
          <ScrollView style={styles.container}>
           <View style={styles.spacing}>
               <Text style={styles.font}>Welcome John, </Text>
               <Text style={styles.font}>Introduction about Destination 21. As you all know we are moving ourselves towards the journey of Destination 21, our three year vision to be in the Top 10th percentile in customer experience, employee experience and returns to financial stakeholders Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta,  
Lorem </Text>
          </View>
          <View style={styles.flatListContainer}>
              <FlatList
                 data={data}
                 renderItem={this.renderItem}
                // numColumns={numColumns}
                 />
            </View>
            <Text style={styles.featuredStories}>FEATURED STORIES</Text>
            {
            this.state.dataSource.length > 2 ? 
            <View>
            <FlatList
              data={this.state.dataSource.slice(0, this.state.itemCount)}
              renderItem={this._renderItem}
              keyExtractor={this.myKeyExtractor}
              ItemSeparatorComponent={this.renderSeperator}
              scrollEnabled={false}
              initialNumToRender={0}
            />
            <Text style={styles.seeMore} onPress={() => Alert.alert("See more")}>See More...</Text>
            </View>
            :
            <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={this.myKeyExtractor}
            ItemSeparatorComponent={this.renderSeperator}
            scrollEnabled={false}
          />
            }
            <this.renderSeperator/>
            <View style={styles.joinNowCard}>
              <Text style={styles.cardText}>Question hour with Vinod</Text>
              <MaterialIcon name='mic' size={90} color='#ffffff' style={{marginTop:10}}/>
              <View style={{alignSelf:'flex-end',marginRight:20,marginBottom:40,flex:1,flexDirection:'row'}}>
              <Text style={styles.cardText}>JOIN NOW </Text>
              <MaterialIcon name='chevron-right' size={40}  color='#ffffff' style={{alignSelf:'center'}}/>
              </View>
            </View>
            <this.renderSeperator/>
            <View styles={{padding:40}}>
            <Text style={[styles.featuredStories,{margin:20}]}>WHAT'S NEW</Text>
            
             <FlatList 
                 data={stories}
                 renderItem={this.renderItemStories}
                 />
                 </View>
                 
          </ScrollView>
      );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor:backgroundColor

  },
  list: {
    alignItems: 'center',
    fontSize: 20,
    color: '#1EDE87',
    justifyContent: 'center',
    backgroundColor: '#F5FCF0',
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 10,
    marginBottom:8,
    marginLeft:10,
    marginRight:10
  },
  image: {
    width: null,
    height: 175,
    margin: 10
  },
  description: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    fontSize: 18,
    color: '#3D86C6',
    lineHeight:30
  },
  buttonsRow:
  {
    flexDirection: 'row',
    margin: 10,
  },
  likeAndComment:
  {
    flex: 1,
    flexDirection: 'row',
  },
  share:
  {
    alignSelf:'center',
  },
  buttonContainer:
  {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  buttonIcon:
  {
    margin: 10,
    color: '#3D86C6'
  },
  buttonText:
  {
    fontSize: 18,
    color: '#3D86C6',
    marginRight:10
    
  },
  seeMore: {
    alignSelf: "center",
    marginBottom: 20,
    marginTop:5,
    fontSize: 16,
    color:'#3D86C6'
  },
  featuredStories:
  {
    fontSize: 20,
    marginLeft: 20,
    color: '#8E8E8E',
    fontWeight: 'bold',
    margin: 10
  },
joinNowCard:
{
  height:220,
  borderWidth:0,
  backgroundColor:'#0073aa',//# 005780
  padding :10,
  borderRadius:10,
  alignSelf:'stretch',
  marginTop:20,
  marginBottom:20,
  marginLeft:20,
  marginRight:20

},
cardText:
{
color:'white',
fontWeight:'bold',
fontSize:26,
alignSelf:'center',
margin:10
},
spacing:
  {
    margin:5
  },
  font:
  {  color:'#8E8E8E',
     fontSize:18,
     margin: 5,
     
  },
  flatListContainer:
  {
    marginTop:10,
    marginBottom:10
  }
}
);

