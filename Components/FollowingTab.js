import React, {Component} from 'react';
import {View,AppRegistry,Text,Button,StyleSheet,FlatList,Image} from 'react-native'
const stories = [
    { key: 'David James',
      subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
      time:'Jan 5'
    }, 
    {
     key: 'David James',
    subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
    time:'Jan 5' 
    }, 
    {
     key: 'David James',
    subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
    time:'Jan 5'
    } ,
    {
        key: 'David James',
       subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
       time:'Jan 5'
       } ,
       {
        key: 'David James',
       subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
       time:'Jan 5'
       } ,
       {
       key: 'David James',
       subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
       time:'Jan 5'
       } ,
       {
        key: 'David James',
        subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time:'Jan 5'
        } ,
        {
            key: 'David James',
            subtitle:'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
            time:'Jan 5'
            } 
    
  ];
export default class Following extends Component{
 renderItemStories = ({ item, index }) => {
        return (
            <View style={{}}> 
              <View style={{flexDirection:'row',marginLeft:15,marginRight:15}}> 
                <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}
                style={{width: 70, height: 70,borderRadius:70/2,alignSelf:'center'}} />
                <View style={{flex:1,marginTop:25,marginLeft:10}}>
                    <View style={{flexDirection:'row',flex:1}}>
                        <View style={{flexDirection:'row',flex:1}}>
                            <Text style={{fontSize:20,fontWeight:'bold'}} >{item.key} </Text>
                        </View>  
                        <View style={{flexDirection:'row',flex:1,justifyContent:'flex-end'}}>
                            <Text style={{fontSize:18,color:'#666479',paddingLeft:10}} >{item.time} </Text>
                        </View>    
                    </View>       
                    <View style={{flexDirection:'column',flex:1}}>
                        <Text style={{fontSize:16,marginBottom:15,color:'#666479'}}  >{item.subtitle} </Text>
                    </View>
                </View>
              </View>
              <View style={{borderBottomWidth:1,borderBottomColor:'#CFCFD3'}}></View>
            </View>
        );
      };
  render()
  {
    return(
      <View style={{flex:1}}>
       <FlatList
                 data={stories}
                 renderItem={this.renderItemStories}
                 
                 />
        
        </View>
      
      
     
    
   )

    
  }
}