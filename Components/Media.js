import React, {Component} from 'react';
import { StyleSheet, Text, View,ViewTouchableHighlight,Alert,TextInput,Button,Switch,FlatList,Image,TouchableOpacity,Dimensions} from 'react-native';
import Swiper from 'react-native-swiper';
import Video from 'react-native-video';
import {AppRegistry} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createDrawerNavigator,DrawerActions} from 'react-navigation'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const mediaContent = [
  { key: 'Video Title goes here',
    description:'Video Description here Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  }, 
  { 
    key: 'Video Title goes here',
    description:'Video Description here Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  },  
];
export default class Media extends Component{
  _renderItem = ({ item }) => {
    return (
      <View style={{marginLeft:25,marginRight:25,marginTop:20}}>
        <Text style={[styles.buttonText,{fontWeight:'bold'}]}>{item.key}</Text>      
        <Text style={styles.description} >{item.description}</Text>
        <Swiper style={styles.image}>
        <View >
          <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}}  style={{width:Dimensions.get('window').width,height:Dimensions.get('window').height/3}}></Image>
        </View>
        <View>
        <Video style={{width:null, height:Dimensions.get('window').height/3}}
          source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }}
          rate={1.0}
          volume={1.0}
          muted={false}
          resizeMode="cover"
          repeat
        />
        </View>
        <View style={{}}>
          <View style={{width:null,height:Dimensions.get('window').height/3,backgroundColor:'yellow'}}resizeMode='stretch'></View>
        </View>
        </Swiper>
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => Alert.alert('Alert', "908 people liked it!!")}>
            <Ionicons name='ios-heart' size={18} style={styles.buttonIcon} />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>908</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonContainer} >
            <MaterialIcon name='comment' size={18} style={styles.buttonIcon} onPress={() => Alert.alert('Alert', "456 comments!!")} />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>456</Text>
          </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText]}> SHARE</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  static navigationOptions = ({ navigation }) => {
    return {
        headerTitle: 'MEDIA',
        headerTitleStyle:{ color:'#0097e6'},
        headerLeft:(
          <View style={{margin:10}}>
            <MaterialIcon
                onPress={navigation.toggleDrawer}
                name="menu"
                size={25}
                color='#0097e6'
            />
            </View>
        ),
        headerRight:
       ( <View style={{margin:20}}>
        <MaterialIcon
            onPress={navigation.toggleDrawer}
            name="search"
            size={25}
            color='#0097e6'
        />
        </View>)
    }
  }
  render() {
    return(
      <View style={styles.container}>
            <FlatList
              data={mediaContent}
              renderItem={this._renderItem}
            />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  postContainer:
  {
      borderWidth:1,
      backgroundColor:'#EFEFEF',
      margin:15,
      flexDirection:'row',
      alignItems:'center'
  },
    image: {
      width:null,
      height: Dimensions.get('window').height/3,
      alignContent: 'stretch',
      marginTop:20,
      marginBottom:20,
      flex:1,
      
    },
    description: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
      fontSize: 18,
      color: '#3D86C6',
      marginTop:5,
    },
    buttonsRow:
    {
      flexDirection: 'row',
      marginBottom:30,
      
    },
    likeAndComment:
    {
      flex: 1,
      flexDirection: 'row'
    },
    share:
    {
      alignSelf:'center',
    
    },
    buttonContainer:
    {
      flexDirection: 'row',
      alignItems: 'center',
      alignContent: 'center',
    
    },
    buttonIcon:
    {
      margin: 5,
      color: '#3D86C6',
      
    },
    buttonText:
    {
      fontSize: 18,
      color: '#3D86C6',
      marginRight:10,
    },
  }
);
