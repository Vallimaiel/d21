import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,FlatList,Image,Dimensions} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export default class CustomerSuccess extends Component
{
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'CUSTOMER SUCCESS',
            headerTitleStyle:{ color:'#3D86C6',flex:1,textAlign:'center'},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:'#ebf7ff'
            },
            headerLeft:(
              <TouchableOpacity style={{padding:10}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={25}
                    color='#3D86C6'
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{padding:20}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={25}
                color='#3D86C6'
            />
            </TouchableOpacity>)
        }
      }
    render()
    {
        return(
            <ScrollView style={styles.container}>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Customer Success</Text>
                <View style={{flex:1}}>
                <Text style={{fontSize:16,color:'#8E8E8E',textAlign:'right',marginRight:10}}>Target 70</Text> 
                </View>
                </View>

                <View style={{flex:1,flexDirection:'row',marginTop:20,marginLeft:20,marginRight:20,alignSelf:'center'}}>
               
               <View style={{flex:1}}>
                    <View  style={{flex:1,flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
                    <MaterialIcon name='person' size={60} color='#8E8E8E' style={{alignSelf:'flex-start'}}/>
                    <View style={{width:60,height:60,borderRadius:30,borderWidth:1,borderColor:'black'}}>
                    <Text style={{fontWeight:'bold',fontSize:30,color:'black',textAlign:'center',margin:7.5}}>5</Text>
                    </View>
                    </View>
                    <Text style={{fontSize:18,color:'black',textAlign:'center',margin:10}}>New Customers</Text>
                    </View>

                    <View style={{height: '60%', width:1, backgroundColor: '#8E8E8E', alignSelf: 'center' ,marginLeft:20,marginRight:20}}/>
                    <View style={{flex:1}}>
                    <View  style={{flex:1,flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
                    <MaterialIcon name='sentiment-satisfied' size={60} color='grey' />
                    <Text style={{fontWeight:'bold',fontSize:30,color:'black',textAlign:'center',margin:10}}>45</Text>
                    </View>
                    <Text style={{fontSize:18,color:'black',textAlign:'center',margin:10}}>Current NPS</Text>
                    </View>
                </View>
                <Text style={[{fontSize:16,color:'#8E8E8E',marginLeft:10,marginRight:10,marginBottom:10}]}>Lorem ipsum dolor sit amet, conse ctetur adipisicing</Text>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>NPS actuals across LOBS</Text>
                <View style={{flex:1}}>
                <Text style={{fontSize:16,color:'black',textAlign:'right',marginRight:10,fontWeight:'bold'}}>TCL GLOBAL - 65</Text> 
                </View>
                </View>
                <Text>SAMPLE TEXT</Text>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Top Promoters</Text>
                </View>
                <View style={styles.bodyContainer}>
                <Text style={styles.bodyText}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta."</Text> 
 <Text style={styles.bodyText}> - John Doe- (Manager, Mahindra group of companies)</Text>
            </View>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Customer wins</Text>
                </View>
                <View style={styles.bodyContainer}>
                <Text style={styles.bodyText}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta."</Text> 
            </View>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Innovation & Transformation</Text>
                </View>
                <View style={[styles.bodyContainer,{paddingBottom:0}]}>
                <View style={{flex:1,flexDirection:'row',margin:20}}>
            <Image style={{backgroundColor:'#b7cff7',width:100,height:100,marginTop:5}}/>
            <Text numberOfLines={3}  style={{flex:1,flexWrap:'wrap',marginLeft:20,alignSelf:'flex-start',fontSize:16,color:'#3D86C6',lineHeight:30}}  >
             <Text style={{fontWeight:'bold' }}>Title </Text>
             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</Text>
            </View>
            <View style={{ width: '80%', height: 1, backgroundColor: '#E3E1E1', alignSelf: 'center' }}/>
            <View style={{flex:1,flexDirection:'row',margin:20}}>
            <Image style={{backgroundColor:'#b7cff7',width:100,height:100,marginTop:5}}/>
            <Text numberOfLines={3}  style={{flex:1,flexWrap:'wrap',marginLeft:20,alignSelf:'flex-start',fontSize:16,color:'#3D86C6',lineHeight:30}} >
            <Text style={{fontWeight:'bold' }}>Title </Text>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</Text>
            </View>
            </View>
            </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container:
    {
      flex:1,
      paddingBottom:40
    },
    cellContainer: {
        flex:1,
        borderWidth:1,
        borderColor:'#E3E1E1',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    },
    cellHeader:
    {
        flex:1,
        flexDirection:'row',
        backgroundColor:'#E3E1E1',
        height:50,
        padding:10,
    },
    leftHeaderText:
    {
        fontSize:16,
        color:'black'
    },
    bodyContainer:
    {
        flex:1,
        paddingBottom:20,
    },
    bodyText:
    {
        fontSize:16,
        color:'black',
        marginTop:20,
        marginLeft:10,
        marginRight:10
    }
})