import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,FlatList,Image,Dimensions} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { createAppContainer,createMaterialTopTabNavigator,NavigationActions,createBottomTabNavigator } from 'react-navigation'

class UpcomingScreen extends React.Component {
    render() {
      return (
            <ScrollView style={styles.container}>
        <View style={styles.cellsContainer}>
            <View style={styles.dateContainer}>
             <Text style={styles.dateText} textAlign='center'>16</Text>
            <Text style={styles.dateText} textAlign="center">OCT</Text>
            </View>
            <View style={styles.eventsCell}>
            <View>
            <Text style={[styles.eventType,{color:'#3D86C6'}]}>Event Name here</Text>
            <Text style={styles.eventType}>Event type</Text>
            </View>
            <Text  numberOfLines={2} style={styles.eventDescription}>Description here Lorem ipsum dolor sit amet, consectetur  </Text>
            </View> 
        </View>
        <View style={styles.seperator}/>
        <View style={styles.cellsContainer}>
            <View style={styles.dateContainer}>
             <Text style={styles.dateText} textAlign='center'>30</Text>
            <Text style={styles.dateText} textAlign="center">OCT</Text>
            </View>
            <View style={styles.eventsCell}>
            <View>
            <Text style={[styles.eventType,{color:'#3D86C6'}]}>Event Name here</Text>
            <Text style={styles.eventType}>Event type</Text>
            </View>
            <Text  numberOfLines={2} style={styles.eventDescription}>Description here Lorem ipsum dolor sit amet, consectetur  </Text>
            </View> 
        </View>
        <View style={styles.seperator}/>
        </ScrollView>

      );
    }
  }
  
  class PastScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Settings!</Text>
        </View>
      );
    }
  }
  class CustomTab extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            menu:'UPCOMING'
        }}

    navigateToScreen = (route) =>  {
       const navigateAction = NavigationActions.navigate({
         routeName: route
       });
        this.state.menu=route
        console.log(this.state.menu)
        this.props.navigation.navigate(navigateAction);
      }
  render() {
    return (
        <View style={{height:40}}>
    <View style = {{flex:1,flexDirection:'row'}}>
     <TouchableOpacity  style={[styles.test,(this.state.menu =='UPCOMING' ? backgroundColor='red' : backgroundColor='#57606f')]}
     onPress={()=>{
         this.navigateToScreen('UPCOMING')
     
     }}>
   <Text style={[(this.state.menu =='UPCOMING' ? Color='black' : backgroundColor='white'),{fontSize:20,textAlign:'center'}]}>UPCOMING</Text></TouchableOpacity>
    <TouchableOpacity  style={[styles.test,(this.state.menu =='PAST' ? backgroundColor='#ced6e0' : backgroundColor='#57606f')]}
     onPress={()=>{this.navigateToScreen('PAST')
     console.log(this.state.menu)
     }}>
    <Text style={[{fontSize:20,textAlign:'center'},(this.state.menu =='PAST' ? Color='black' : backgroundColor='white')]}>PAST</Text></TouchableOpacity>
    </View> 
    </View>
    )}
    }
  
  const TabNavigator = createMaterialTopTabNavigator({
    UPCOMING: UpcomingScreen,
    PAST: PastScreen,
  }, {
     
    tabBarPosition:'top',
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'grey',
      style:{
        backgroundColor:'white',
        marginTop:10
       },
      indicatorStyle:
      {
          backgroundColor:'#3D86C6'
      }
    },
   // tabBarComponent:CustomTab
    },
);
  
  const TabContainer = createAppContainer(TabNavigator);

export default class Events extends Component
{
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'EVENTS',
            headerTitleStyle:{ color:'#3D86C6',flex:1,textAlign:'center'},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:'#ebf7ff'
            },
            headerLeft:(
              <TouchableOpacity style={{padding:10}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={25}
                    color='#3D86C6'
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{padding:20}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={25}
                color='#3D86C6'
            />
            </TouchableOpacity>)
        }
      }
    
    
render()
{
   
    return(
       <TabContainer/>
    );
}
}
const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    cellsContainer:
    {
        flex:1,
        flexDirection:'row',
        margin:20
    },
    dateContainer:
    {   padding:10,
        borderColor:'#E3E1E1',
        borderWidth:3,
        width:120,
        height:120,
        alignItems:'center'

    },
    dateText:
    {
        color:'#8E8E8E',
        alignSelf:'center',
        textAlign:'center',
        fontSize:32,
        fontWeight:'bold'
    },
    eventsCell:
    {
        flex:1,
        marginLeft:20
    },
    eventType:{
    textAlign:'left',
    fontSize:18
    },
    eventDescription:
    {
        alignSelf:'center',
        fontSize:18,
        marginTop:10
    },
    seperator:{
        width: '100%', 
        height: 5, 
        backgroundColor: '#E3E1E1',
        alignSelf: 'center'
    },
    test:
    {  
        flex:1,
        padding:10
    }
})