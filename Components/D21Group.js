import React, {Component} from 'react';
import {View,AppRegistry,Text,Image,Button,StyleSheet} from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import {  createAppContainer,createMaterialTopTabNavigator,tabBarOptions } from 'react-navigation';
import All from './AllTab';
import Following from './FollowingTab';

export default class Group extends Component{
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: 'D21 GROUP',
            headerTitleStyle:{ color:'#0097e6'},
            headerLeft:(
              <View style={{margin:10}}>
                <MaterialIcon
                    onPress={navigation.toggleDrawer}
                    name="menu"
                    size={25}
                    color='#0097e6'
                />
                </View>
            ),
            headerRight:
           ( <View style={{margin:20}}>
            <MaterialIcon
                onPress={navigation.toggleDrawer}
                name="search"
                size={25}
                color='#0097e6'
            />
            </View>)
        }
      }
    render()
    {
      return(
        <View style={styles.container}>
        <View style={styles.container2}>
        <MaterialIcon name='edit' size={30} style={alignSelf='flex-start'} color='#466FF7'/>
        <View>
         <Text style={{textAlign:'center'}}>Inspire your group, Add your story now</Text>
         </View>
        </View>
         <ToptabNavigator/>
        </View>
       
      
     )
  
      
    }
  }
  const TabNavigator = createMaterialTopTabNavigator({
    
    All: { screen: All},
    Following: { screen:Following  }},
    {
    tabBarOptions :{
        activeTintColor:'black',
        inactiveTintColor:'grey',
        indicatorStyle:{
          backgroundColor:'#51B5FF'
        },
        style: {
          backgroundColor: 'white',
        }
        
}
    });
  const ToptabNavigator = createAppContainer(TabNavigator);
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      
    },
    container2:
    {
        borderWidth:1,
        alignItems:'center',
        marginTop:10,
        marginLeft:15,
        marginRight:15,
        marginBottom:15,
        flexDirection:'row',
        backgroundColor:'#EFEFEF'
    }
    
  });