/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import { createStackNavigator,createAppContainer, } from 'react-navigation'
import MyDrawerNavigation from './Components/MyDrawerNavigation';
import Events from './Components/Events';
import Search from './Components/Search';
import Login from './Components/Login';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
 const AppNavigator = createStackNavigator(
  {
    Home:{
      screen:Login,
      navigationOptions:{
        header:null
      }
    },
    MyDrawerNavigation:
    {
      screen:MyDrawerNavigation, 
      navigationOptions:{
      header:null
    }
      },
    Search:
    {
      screen:Search,
      navigationOptions: () => ({
             
            headerTitle: 'Search Results',
            headerTitleStyle:{ color:'#3D86C6'},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:'#ebf7ff',
            },
            headerTintColor: '#3D86C6', 
          }),
      }
  },
  {
    initialRouteName : 'Home',
    //headerMode:'None'
  }
);  

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}